typedef struct _realNumber {
       unsigned value; 
} realNumber;


extern int isZero(realNumber obj);
extern realNumber createRealNumber(int, int);
extern int isPositive(realNumber);


