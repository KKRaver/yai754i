#include "real_number.h"

int isPositive(realNumber obj) {
        return obj.value >> 31 == 0;
}

int isZero(realNumber obj) {
        return obj.value == 0;
}


realNumber createRealNumber(int before, int after) {
        realNumber rn = {.value = before | after};
        return rn;
}
