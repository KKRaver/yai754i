#include "real_number.h"
#include <assert.h>



void testInstantiate(int, int, int);
void testSign();


int main() {
        testInstantiate(1, 0, 0);
        testInstantiate(0, 1, 0);
        testInstantiate(0, 0, 1);
        testSign(); 
        return 0; 
}

void testInstantiate(int shouldBeTrue, int before, int after) {
        realNumber obj = createRealNumber(before, after);
        assert(shouldBeTrue == isZero(obj));
}




void testSign() {
        realNumber obj = createRealNumber(1, 0);
        assert(isPositive(obj));
        obj = createRealNumber(-1, 0);
        assert(!isPositive(obj));
}


