CFLAGS = -Wall -Werror -Wextra -pedantic --std=gnu99

lib: real_number.o

test: main.o lib
	cc -o main main.o real_number.o	
	./main

clean:
	rm *.o main
